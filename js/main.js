//центрирование картинок
$(window).load(function() {
    $(".front img").imgCenter();
});

jQuery(function($) {
    $('.block-1 .SimSlider').SimSlider({
        next: '.sliderBox .nextBtn', // селектор стрелки вправо
        prev: '.sliderBox .prevBtn', // селектор стрелки влево
        item: '.SimSlider .slide', // селектор слайда
        speed: 1200, // скорость прокрутки
        delay: 5000 // задержка в мс
    });
});


$(document).ready(function() {
    $('.wrap-item .back').hide().css('left', 0);

    function mySideChange(front) {
        if (front) {
            $(this).parent().find('div.front').show();
            $(this).parent().find('div.back').hide();

        } else {
            $(this).parent().find('div.front').hide();
            $(this).parent().find('div.back').show();
        }
    }

    $('.slide ul  li').hover(
        function() {
            $(this).find('wrap-item').stop().rotate3Di('flip', 250, {
                direction: 'clockwise',
                sideChange: mySideChange
            });
        },
        function() {
            $(this).find('wrap-item').stop().rotate3Di('unflip', 500, {
                sideChange: mySideChange
            });
        }
    );
});